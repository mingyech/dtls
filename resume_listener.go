// SPDX-FileCopyrightText: 2023 The Pion community <https://pion.ly>
// SPDX-License-Identifier: MIT

package dtls

import (
	"net"

	"github.com/pion/dtls/v2/internal/net/udp"
	dtlsnet "github.com/pion/dtls/v2/pkg/net"
)

// Listen creates a DTLS listener
func NewResumeListener(network string, laddr *net.UDPAddr, config *Config) (dtlsnet.PacketListener, error) {
	lc := udp.ListenConfig{
		DatagramRouter:       cidDatagramRouter(len(config.ConnectionIDGenerator())),
		ConnectionIdentifier: cidConnIdentifier(),
	}
	return lc.Listen(network, laddr)
}
